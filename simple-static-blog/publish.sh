#!/bin/bash

#ARTICLES=($(ls -cr1 ./articles/))    # change time order
ARTICLES=($(ls -1 ./articles/))       # filename order

if [ $# == 1 ] && [ $1 == 'all' ]; then
    rm -f ./htmls/*
elif [ -e "./articles/$1" ]; then
    FILENAME="${1%.*}"
    rm -f "./htmls/$FILENAME.html"
fi

ARTICLE_LIST_HTML="<meta charset='UTF-8'>"
FILENAME_PREV=''
TITLE_PREV=''

for ARTICLE in "${ARTICLES[@]}"
do
    FILENAME="${ARTICLE%.*}"

    LINES=`wc -l ./articles/${FILENAME}.txt | cut -d' ' -f1`
    CONTENT_LENGTH=`expr $LINES - 1`

    DATE=`ls -l --time-style=long-iso ./articles/${FILENAME}.txt | cut -d' ' -f6-7`
    TITLE=`head -1 ./articles/${FILENAME}.txt`
    CONTENT=`tail -$CONTENT_LENGTH ./articles/${FILENAME}.txt`

    ARTICLE_LIST_HTML="$ARTICLE_LIST_HTML""
    <li> $DATE -
      <a href='./htmls/${FILENAME}.html' style='text-decoration:none; color:#333;'> $TITLE </a>
    </li>"

    if [ ! -e "./htmls/${FILENAME}.html" ]; then

        #echo "TITLE: $TITLE"
        #echo "CONTENT: $CONTENT"
        echo "$FILENAME: $TITLE"

        ARTICLE_HTML=''
        ARTICLE_HTML="$ARTICLE_HTML""
        <html>
          <head>
            <meta charset='UTF-8'>
            <title> $TITLE </title>

            <!-- Google Fonts -->
            <link href='http://fonts.googleapis.com/css?family=Dancing+Script' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/earlyaccess/jejumyeongjo.css' rel='stylesheet' type='text/css'>

            <style>
              body {
                color: #333;
                font-size: 12pt;
              }

              pre {
                font-size: 15pt;
                font-family: 'Jeju Myeongjo', serif;
              }

              a {
                color: #666;
              }

              div.separator {
                width: 960px;
                margin-top: 2px;
                margin-bottom: 2px;
                border-top: 1px solid #666;
              }
            </style>
          </head>
          <body>
            <div style='width:960px; margin:0 auto;'>
              <div style='width:100%; position:relative; height:25px;'>
                <div style='position:absolute; left:0px;'>$DATE</div>
                <div style='position:absolute; right:0px;'>
                  <a href='../list.html'> 목록 </a>
                </div>
              </div>

              <div class='separator'></div>
              <h1>$TITLE</h1>
              <pre style='line-height:1.5em; white-space: pre-line'>
                $CONTENT
              </pre>
              <div class='separator' style='margin-top:20px; margin-bottom:10px;'></div>
        "

        if [ ! $FILENAME_PREV == '' ]; then
            ARTICLE_HTML="$ARTICLE_HTML""
            <a href='./${FILENAME_PREV}.html'> &rarr; $TITLE_PREV </a>
            "
        fi

        ARTICLE_HTML="$ARTICLE_HTML""
           </div>
          </body>
        </html>
        "

        # store article html file
        echo "$ARTICLE_HTML" > "./htmls/${FILENAME}.html"
    fi

    FILENAME_PREV="$FILENAME"
    TITLE_PREV="$TITLE"
done

# store article list html file
echo "$ARTICLE_LIST_HTML" > "./list.html"

# make index page
if [ ! $FILENAME_PREV == '' ]; then
    rm -f index.html
    echo "
    <html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
      <title></title>
      <meta http-equiv='refresh' content='0;URL=./htmls/${FILENAME_PREV}.html' />
    </head>
    <body>
      <p></p> 
    </body>
    </html>
    " > index.html
fi
