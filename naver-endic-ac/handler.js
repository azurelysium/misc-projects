const DEFINITION_ID = 'firefox_extension_naver_endic_ac';

const showDefinition = (text) => {
  let elem = document.createElement('div');
  elem.id = DEFINITION_ID;
  elem.innerHTML = text;

  elem.style.setProperty('position', 'fixed', 'important');
  elem.style.setProperty('top', '10px', 'important');
  elem.style.setProperty('left', '10px', 'important');
  elem.style.setProperty('display', 'block', 'important');
  elem.style.setProperty('width', 'fit-content', 'important');
  elem.style.setProperty('width', '-moz-fit-content', 'important');
  elem.style.setProperty('width', '-webkit-fit-content', 'important');
  elem.style.setProperty('height', 'auto', 'important');
  elem.style.setProperty('z-index', `${Number.MAX_SAFE_INTEGER}`, 'important');
  elem.style.setProperty('background-color', '#ffc', 'important');
  elem.style.setProperty('color', 'black', 'important');
  elem.style.setProperty('font', 'normal 12px sans-serif', 'important');
  elem.style.setProperty('border', '3px solid gray', 'important');
  elem.style.setProperty('margin', '0', 'important');
  elem.style.setProperty('padding', '5', 'important');

  document.body.appendChild(elem);
};

const hideDefinition = () => {
  const elem = document.getElementById(DEFINITION_ID);
  if (elem) {
    document.body.removeChild(elem);
  }
};

const getWordMeaning$ = (word) => {
  let url = `https://ac.endic.naver.com/ac?q_enc=utf-8&st=1100&r_format=json&r_enc=utf-8&q=${word}`;
  return Rx.Observable.fromPromise(fetch(url))
    .mergeMap(data => Rx.Observable.fromPromise(data.text()))
    .map(data => JSON.parse(data))
    .map((response) => {
      let result = '';
      response.items.forEach((row) => {
        row.forEach((entry) => {
          result += `
          <tr>
          <td style='font-size:8pt; color:black;'> ${entry[0]} </td>
          <td style='font-size:8pt; color:black;'> ${entry[1]} </td>
          </tr>`;
        });
      });
      result = '<table>' + result + '</table>';
      return result;
    });
};

const obs = Rx.Observable.fromEvent(document, 'mouseup')
  .mergeMap((ev) => {
    hideDefinition();
    let word = document.getSelection().toString().trim();
    if (!word) {
      return Rx.Observable.empty();
    }
    return getWordMeaning$(word);
  })
  .do((text) => {
    showDefinition(text);
  });

obs.subscribe();
